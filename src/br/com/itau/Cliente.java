package br.com.itau;

public class Cliente {
    private String nome;
    private String cpf;
    private int idade;

    public Cliente(String nome, String cpf, int idade) throws Exception {
        this.nome = nome;
        this.cpf = cpf;
        setIdade(idade);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) throws Exception {
        if (idade >= 18)
            this.idade = idade;
        else throw new Exception("Cliente não pode ter menos de 18 anos. Tente novamente.");
    }
}
