package br.com.itau;

import java.util.Scanner;

public class IO {

    private static Scanner scanner = new Scanner(System.in);

    public static void inicializarSistema() {
        System.out.println("Bem vindo!");
    }

    public static Cliente solicitarDadosCliente() throws Exception {

        String nome;
        String cpf;
        int idade;

        System.out.println("Digite o cpf do cliente: ");
        cpf = scanner.next();
        System.out.println("Digite o nome do cliente: ");
        nome = scanner.next();
        System.out.println("Digite a idade do cliente: ");
        idade = scanner.nextInt();

        return new Cliente(nome, cpf, idade);
    }

    public static void menu() throws Exception {
        System.out.println("Digite o saldo inicial do Cliente");
        double saldoInicial = scanner.nextDouble();

        ContaBancaria conta = new ContaBancaria(solicitarDadosCliente(), saldoInicial);
        String saida = "";

        while (!saida.equalsIgnoreCase("q")) {
            System.out.println("**********Menu**********");
            System.out.println("1 - Sacar");
            System.out.println("2 - Depositar");
            System.out.println("3 - Consultar saldo");
            System.out.println("4 - Sair");
            System.out.println("Digite o numero do menu que deseja utilizar");

            switch (scanner.nextInt()) {
                case 1: {
                    System.out.println("Digite o valor que quer sacar");
                    System.out.println(conta.Sacar(scanner.nextDouble()));
                } break;
                case 2: {
                    System.out.println("Digite o valor que quer sacar");
                    System.out.println(conta.Depositar(scanner.nextDouble()));
                } break;
                case 3: {
                    System.out.println("Saldo: " + conta.getSaldo());
                } break;
                case 4: saida = "q"; break;
            }

        }
    }

}
