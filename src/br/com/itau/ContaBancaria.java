package br.com.itau;

public class ContaBancaria {
    private Cliente cliente;
    private double saldo;

    public ContaBancaria(Cliente cliente, double saldo) {
        this.cliente = cliente;
        this.saldo = saldo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public double getSaldo() {
        return saldo;
    }

    public String Sacar(double valor) {
        if ((saldo - valor) >= 0) {
            saldo = saldo - valor;
            return "Valor retirado com sucesso.";
        } else return "Não há saldo sulficiente. Saldo: " + saldo;
    }

    public boolean Depositar(double valor) {
        saldo = saldo + valor;
        return true;
    }
}
