package br.com.itau;

public class Main {

    public static void main(String[] args) {
        // write your code here
        try {
            IO.inicializarSistema();
            IO.menu();
        } catch (Exception e) {
            System.out.println("Erro: " + e.getMessage());
        }
    }
}
